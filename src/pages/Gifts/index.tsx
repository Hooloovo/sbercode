import React from 'react';
import { Container, Header, HeaderContent, MainTab } from "../Stream/styled";
import { Gift, GiftTable } from "./styled";

import a from './img/1.png'
import b from './img/2.png'
import c from './img/3.png'

export const GiftPage = () => {
  return (
    <Container>
      <Header>
        <HeaderContent>
          <MainTab active={true}>Подарочки</MainTab>
        </HeaderContent>
      </Header>
      <GiftTable>
        <Gift src={a} style={{cursor: 'pointer'}}/>
        <Gift src={b}/>
        <Gift src={c}/>
      </GiftTable>
    </Container>
  )
}
