import React, { useState } from "react";
import {
  Container,
  Header,
  HeaderContent,
  MainTab,
  SearchContainer,
  StreamTable,
  SearchTab,
  BankContainer, TabsContainer, CreateArticleButton
} from "../Personal/styled";
import { Post } from "../../components/Post";
import { Interested } from "../../components/Interested";
import { EditPost } from "../../components/EditPost";
import { useHistory, useParams } from "react-router-dom";
// import { EditPost } from "../../components/EditPost";
import { useQuery } from "@apollo/client";
import { GET_ARTICLE, GET_BLACKNOTES } from "../../api";

export const EditPage = () => {
  const history = useHistory();
  // @ts-ignore
  let { id } = useParams();
  const { loading, error, data, refetch: refetchBlack } = useQuery(GET_ARTICLE, { variables: {id: id}});

  return (
    <Container>
      <Header>
        <HeaderContent>
          <TabsContainer>
            <MainTab>Мои статьи</MainTab>
            <MainTab>Черновики</MainTab>
            <MainTab>Модерация</MainTab>
            <MainTab>Банк идей</MainTab>
          </TabsContainer>
          <CreateArticleButton onClick={() => history.push('/create')}>Написать статью</CreateArticleButton>
        </HeaderContent>
      </Header>

      {!loading && <EditPost content={JSON.parse(data.getPost.content)} mainTitle={data.getPost.title}/>}
    </Container>
  )
}
