import React, { useState } from "react";
import {
  Container,
  Header,
  HeaderContent,
  MainTab,
  SearchContainer,
  StreamTable,
  SearchTab,
  BankContainer, TabsContainer, CreateArticleButton
} from "../Personal/styled";
import { Post } from "../../components/Post";
import { Interested } from "../../components/Interested";
import { EditPost } from "../../components/EditPost";
import { useHistory, useParams } from "react-router-dom";
// import { EditPost } from "../../components/EditPost";
import { useQuery } from "@apollo/client";
import { GET_ARTICLE, GET_BLACKNOTES } from "../../api";

export const CreatePage = () => {
  const history = useHistory();
  // @ts-ignore

  return (
    <Container>
      <Header>
        <HeaderContent>
          <TabsContainer>
            <MainTab>Мои статьи</MainTab>
            <MainTab>Черновики</MainTab>
            <MainTab>Модерация</MainTab>
            <MainTab>Банк идей</MainTab>
          </TabsContainer>
          <CreateArticleButton onClick={() => history.push('/create')}>Написать статью</CreateArticleButton>
        </HeaderContent>
      </Header>

      <EditPost/>
    </Container>
  )
}
