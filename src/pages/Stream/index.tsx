import React, { useState } from "react";
import {
  Container,
  Header,
  HeaderContent, Hr,
  MainTab, MDesc, MetricContainer, MetricCount, MetricLine, MetricTab, PointName, PointPercent, PointText,
  SearchContainer,
  SearchInput,
  SearchTab,
  StreamTable
} from "./styled";
import { SimpleContainer } from '../../components/basic/Container/styled';
import { Title } from "../../components/typography";
import { Search } from "../../static/icons";
import { Post } from "../../components/Post";
import { ReactComponent as FilterIcon } from "./img/filter.svg";
import metric1 from './img/metric.svg';
import metric2 from './img/metric2.svg';
import { useMutation, useQuery } from '@apollo/client';
import { CREATE_OFFER, GET_OFFER, GET_POSTS, GET_STATISTICS, GET_USER_INFO, PUBLISH_ARTICLE } from "../../api";
import { Poll } from "../../components/Poll";
import { Offer } from "../../components/Offer";
import Modal from '@material-ui/core/Modal';
import { ModalBlock } from "../../components/Modal";


export const StreamPage = () => {
  const [currentTab, setTab] = useState<number>(1)
  const [searchTab, setSearchTab] = useState('best')
  const [metricTab, setMetricTab] = useState(1)
  const [open, setOpen] = useState(false)
  const { loading, error, data } = useQuery(GET_POSTS);
  const { loading: loadingStats, error: errorStats, data: dataStats } = useQuery(GET_STATISTICS);
  const { loading: loadingOffer, error: errorOffer, data: dataOffer } = useQuery(GET_OFFER);
  const [createOffer, { data: anotherData }] = useMutation(CREATE_OFFER);

  console.log(111, dataOffer)

  return (
    <Container>
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <ModalBlock />
      </Modal>
      <Header>
        <HeaderContent>
          <MainTab active={currentTab === 1} onClick={() => setTab(1)}>Моя лента</MainTab>
          <MainTab active={currentTab === 2} onClick={() => setTab(2)}>Рекомендации друзей</MainTab>
          <MainTab active={currentTab === 3} onClick={() => setTab(3)}>Назначенные мне</MainTab>
        </HeaderContent>
      </Header>
      <SearchContainer>
        <div>
          <SearchTab active={searchTab === 'best'} onClick={() => setSearchTab('best')}>Лучшие</SearchTab>
          <SearchTab active={searchTab === 'last'} onClick={() => setSearchTab('last')}>Последние</SearchTab>
        </div>
        <div>
          <Search />
          <SearchInput placeholder={'Поиск...'}/>
        </div>
      </SearchContainer>
      <StreamTable>
        <div>
          {!loadingOffer && dataOffer.getNextOffer && <Poll title={dataOffer.getNextOffer.title} id={dataOffer.getNextOffer.id}/>}
          <Offer/>
          {!loading && data.allPosts.edges.map((elem: any) =>
            <Post
              key={elem.node.id}
              id={elem.node.id}
              title={elem.node.title}
              content={elem.node.content}
              createdOn={elem.node.createdOn}
              commentsCount={elem.node.commentsCount}
              likes={elem.node.likes}
              views={elem.node.views}
              author={elem.node.author}
              preview={elem.node.preview}
              tags={['Разработка', 'JavaScript', 'Браузеры']}
            />
          )}
        </div>
        <div>
          <MetricContainer>
            <div>
              <MetricTab active={metricTab === 1} onClick={() => setMetricTab(1)}>Сегодня</MetricTab>
              <MetricTab active={metricTab === 2} onClick={() => setMetricTab(2)}>Неделя</MetricTab>
              <MetricTab active={metricTab === 3} onClick={() => setMetricTab(3)}>Месяц</MetricTab>
              <MetricTab active={metricTab === 4} onClick={() => setMetricTab(4)}>Год</MetricTab>
            </div>
            <Hr/>
            <div>
              {!loadingStats && <MetricCount>{dataStats.statistics.distance} метра</MetricCount>}
            </div>
            <div>
              <MDesc>статей прочитано</MDesc>
            </div>
            <div>
              <PointText>Цель по направлениям</PointText>
              <FilterIcon onClick={() => setOpen(true)} style={{marginBottom: '24px'}} />
            </div>
            <div>
              <PointName>DevOps</PointName>
              <PointPercent>94%</PointPercent>
            </div>
            <div>
              <MetricLine src={metric1} />
            </div>
            <div>
              <PointName>Менеджмент</PointName>
              <PointPercent>22%</PointPercent>
            </div>
            <div>
              <MetricLine src={metric2} />
            </div>
          </MetricContainer>
        </div>
      </StreamTable>
    </Container>
  )
}
