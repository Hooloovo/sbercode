import React, { useState } from "react";
import {
  Container,
  Header,
  HeaderContent, Hr,
  MainTab, MDesc, MetricContainer, MetricCount, MetricLine, MetricTab, PointName, PointPercent, PointText,
  SearchContainer,
  SearchInput,
  SearchTab,
  StreamTable
} from "./styled";
import { SimpleContainer } from '../../components/basic/Container/styled';
import { Title } from "../../components/typography";
import { Search } from "../../static/icons";
import { Post } from "../../components/Post";
import { ReactComponent as FilterIcon } from "./img/filter.svg";
import metric1 from './img/metric.svg';
import metric2 from './img/metric2.svg';
import { useQuery } from '@apollo/client';
import { GET_POSTS, GET_STATISTICS } from "../../api";
import { Poll } from "../../components/Poll";
import { Offer } from "../../components/Offer";
import { Article } from "../../components/Article";


export const ArticlePage = () => {
  const [currentTab, setTab] = useState<number>(1)
  const [searchTab, setSearchTab] = useState('best')
  const [metricTab, setMetricTab] = useState(1)
  const { loading: loadingStats, error: errorStats, data: dataStats } = useQuery(GET_STATISTICS);
  const { loading, error, data } = useQuery(GET_POSTS);

  console.log(data)
  console.log(error)
  console.log(loading)

  return (
    <Container>
      <Header>
        <HeaderContent>
          <MainTab active={currentTab === 1} onClick={() => setTab(1)}>Моя лента</MainTab>
          <MainTab active={currentTab === 2} onClick={() => setTab(2)}>Рекомендации друзей</MainTab>
          <MainTab active={currentTab === 3} onClick={() => setTab(3)}>Назначенные мне</MainTab>
        </HeaderContent>
      </Header>
      <SearchContainer>
        <div>
          <SearchTab active={searchTab === 'best'} onClick={() => setSearchTab('best')}>Лучшие</SearchTab>
          <SearchTab active={searchTab === 'last'} onClick={() => setSearchTab('last')}>Последние</SearchTab>
        </div>
        <div>
          <Search />
          <SearchInput placeholder={'Поиск...'}/>
        </div>
      </SearchContainer>
      <StreamTable>
        <div>
          <Article />
        </div>
        <div>
          <MetricContainer>
            <div>
              <MetricTab active={metricTab === 1} onClick={() => setMetricTab(1)}>Сегодня</MetricTab>
              <MetricTab active={metricTab === 2} onClick={() => setMetricTab(2)}>Неделя</MetricTab>
              <MetricTab active={metricTab === 3} onClick={() => setMetricTab(3)}>Месяц</MetricTab>
              <MetricTab active={metricTab === 4} onClick={() => setMetricTab(4)}>Год</MetricTab>
            </div>
            <Hr/>
            <div>
              {!loadingStats && <MetricCount>{dataStats.statistics.distance} метра</MetricCount>}
            </div>
            <div>
              <MDesc>статей прочитано</MDesc>
            </div>
            <div>
              <PointText>Цель по направлениям</PointText>
              <FilterIcon style={{marginBottom: '24px'}} />
            </div>
            <div>
              <PointName>DevOps</PointName>
              <PointPercent>94%</PointPercent>
            </div>
            <div>
              <MetricLine src={metric1} />
            </div>
            <div>
              <PointName>Менеджмент</PointName>
              <PointPercent>22%</PointPercent>
            </div>
            <div>
              <MetricLine src={metric2} />
            </div>
          </MetricContainer>
        </div>
      </StreamTable>
    </Container>
  )
}
