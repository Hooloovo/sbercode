import styled from 'styled-components';
import { PALETTE, TEXT } from "../../styles/constants";
import search from '../../static/icons/search.svg';
import { SimpleContainer } from "../../components/basic/Container/styled";

interface ITab {
  active?: boolean,
}

interface ISearchTab {
  active?: boolean,
}

interface IMetricTab {
  active?: boolean
}

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const Header = styled.header`
  width: 100%;
  height: 83px;
  display: flex;
  align-items: baseline;
  border-bottom: 1px solid ${PALETTE.lines};
`

export const HeaderContent = styled.div`
  width: 100%;
  height: 100%;
  padding: 38px 88px 19px 16px;
  display: flex;
  align-items: baseline;
  
`

export const MainTab = styled.button<ITab>`
  border: none;
  background: transparent;
  font-style: normal;
  font-weight: ${props => props.active ? 'bold' : 'normal'};
  font-size: ${props => props.active ? '24px' : '16px'};
  line-height: ${props => props.active ? '29px' : '19px'};
  color: ${TEXT.black};
  margin-right: 25px;
  cursor: pointer;
`

export const SearchContainer = styled.div`
  width: 100%;
  padding: 16px 88px 18px 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
  
  & > div {
    display: flex;
    align-items: center;
  }
`

export const SearchTab = styled.button<ISearchTab>`
  height: 28px;
  border: none;
  background-color: ${props => props.active ? PALETTE.blue : 'transparent'};
  border-radius: 14px;
  display: flex;
  flex-direction: column;
  color: ${props => props.active ? '#FFFFFF' : TEXT.black};
  font-style: normal;
  font-weight: ${props => props.active ? 'bold' : 'normal'};
  font-size: 16px;
  line-height: 19px;
  padding: 4px 16px;
  margin-right: 16px;
  cursor: pointer;
`

export const SearchInput = styled.input`
  width: 100px;
  height: 20px;
  border: none;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 19px;
  color: ${TEXT.black};
  transition-duration: .2s;
  align-self: flex-end;
  margin-left: 18px;
  
  &:focus {
    width: 220px;
  }
  
  &:before {
    width: 17px;
    height: 17px;
    border: 1px;
    content: '';
    background-image: url("${search}");
  }
`;

export const StreamTable = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 0 88px 20px 16px;
  display: grid;
  grid-template-columns: 1fr 304px;
  grid-column-gap: 16px;
  
  & > div {
    display: flex;
    flex-direction: column;
  }
`

export const MetricContainer = styled(SimpleContainer)`
  padding-left: 0;
  padding-right: 0;
  
  > div {
    width: 100%;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 24px;
  }
`

export const MetricTab = styled.button<IMetricTab>`
  border: none;
  height: 24px;
  padding: ${props => props.active ? '4px 13px' : '0'};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 12px;
  
  background-color: ${props => props.active ? PALETTE.blue : 'transparent'};
  color: ${props => props.active ? '#FFF' : TEXT.black};
  font-style: normal;
  font-weight: ${props => props.active ? 'bold' : 'normal'};
  font-size: 14px;
  line-height: 16px;
  cursor: pointer;
`;

export const Hr = styled.div`
  width: 100%;
  height: 1px;
  content: '';
  border-bottom: 1px solid ${PALETTE.lines};
  margin-bottom: 24px;
  margin-top: 16px;
`

export const MetricCount = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 36px;
  color: ${PALETTE.red};
  margin-bottom: 4px;
`

export const MDesc = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.gray};
  
  margin-bottom: 32px;
`

export const PointText = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.black};
  margin-bottom: 24px;
`

export const PointName = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  color: ${TEXT.black};
  margin-bottom: 8px;
`

export const PointPercent = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  color: ${TEXT.gray};
  margin-bottom: 8px;
`

export const MetricLine = styled.img`
  width: 256px;
  height: 10px;
  margin-bottom: 16px;
`
