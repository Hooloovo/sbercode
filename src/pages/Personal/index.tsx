import React, { useEffect, useState } from "react";
import {
  Container,
  Header,
  HeaderContent,
  MainTab,
  SearchContainer,
  StreamTable,
  SearchTab,
  BankContainer, TabsContainer, CreateArticleButton
} from "./styled";
import { Post } from "../../components/Post";
import { Interested } from "../../components/Interested";
import { useMutation, useQuery } from "@apollo/client";
import { GET_BLACKNOTES, GET_MODERATION, GET_POSTS, GET_PUBLISHED, PUBLISH_ARTICLE, UPDATE_ARTICLE } from "../../api";
import { useHistory } from "react-router-dom";
import { RightBlock } from "../../components/RightBlock";

export const PersonalPage = () => {
  const [currentTab, setTab] = useState<number>(1)
  const [searchTab, setSearchTab] = useState('all')
  const { loading: loadingBlack, error: errorBlack, data: dataBlack, refetch: refetchBlack } = useQuery(GET_BLACKNOTES);
  const { loading: loadingModeration, error: errorModeration, data: dataModeration, refetch: refetchModeration } = useQuery(GET_MODERATION);
  const { loading: loadingPublished, error: errorPublished, data: dataPublished, refetch: refetchPublished } = useQuery(GET_PUBLISHED);
  const [publishArticle, { data: someData }] = useMutation(PUBLISH_ARTICLE);

  const history = useHistory();

  useEffect(() => {
    if (currentTab === 1)
      refetchPublished()
    if (currentTab === 2)
      refetchBlack()
    if (currentTab === 3)
      refetchModeration()
    console.log(dataModeration);
  }, [currentTab])

  return (
    <Container>
      <Header>
        <HeaderContent>
          <TabsContainer>
            <MainTab active={currentTab === 1} onClick={() => setTab(1)}>Мои статьи</MainTab>
            <MainTab active={currentTab === 2} onClick={() => setTab(2)}>Черновики</MainTab>
            <MainTab active={currentTab === 3} onClick={() => setTab(3)}>Модерация</MainTab>
            <MainTab active={currentTab === 4} onClick={() => setTab(4)}>Банк идей</MainTab>
          </TabsContainer>
          <CreateArticleButton onClick={() => history.push('/create')}>Написать статью</CreateArticleButton>
        </HeaderContent>
      </Header>
      {currentTab === 4 && <SearchContainer>
        <div>
          <SearchTab active={searchTab === 'all'} onClick={() => setSearchTab('all')}>Все</SearchTab>
          <SearchTab active={searchTab === 'dev'} onClick={() => setSearchTab('dev')}>Разработка</SearchTab>
          <SearchTab active={searchTab === 'design'} onClick={() => setSearchTab('design')}>Дизайн</SearchTab>
          <SearchTab active={searchTab === 'managment'} onClick={() => setSearchTab('managment')}>Менеджмент</SearchTab>
          <SearchTab active={searchTab === 'skills'} onClick={() => setSearchTab('skills')}>Soft-skills</SearchTab>
        </div>
      </SearchContainer>}

      {currentTab !== 4 && <StreamTable>
        <div>
          {currentTab === 1 && !loadingPublished && dataPublished.myPosts.edges.map((elem: any) =>
            <Post
              key={elem.node.id}
              id={elem.node.id}
              title={elem.node.title}
              content={elem.node.content}
              createdOn={elem.node.createdOn}
              commentsCount={elem.node.commentsCount}
              likes={elem.node.likes}
              views={elem.node.views}
              author={elem.node.author}
              tags={['Разработка', 'JavaScript', 'Браузеры']}
              preview={elem.node.preview}
              onDelete={() => ({})}
              // onEdit={() => history.push(`/edit/${elem.node.id}`)}
              // onPublish={() => ({})}
            />
          )}

          {currentTab === 2 && !loadingBlack && dataBlack.myPosts.edges.map((elem: any) =>
            <Post
              key={elem.node.id}
              id={elem.node.id}
              title={elem.node.title}
              content={elem.node.content}
              createdOn={elem.node.createdOn}
              commentsCount={elem.node.commentsCount}
              likes={elem.node.likes}
              views={elem.node.views}
              // author={elem.node.author}
              tags={['Разработка', 'JavaScript', 'Браузеры']}
              preview={elem.node.preview}
              onEdit={() => history.push(`/edit/${elem.node.id}`)}
              onPublish={() => {
                publishArticle({ variables: {postId: elem.node.id}}).then(() => refetchBlack())
              }}
            />
          )}

          {currentTab === 3 && !loadingModeration && dataModeration.myPosts.edges.map((elem: any) =>
            <Post
              key={elem.node.id}
              id={elem.node.id}
              title={elem.node.title}
              content={elem.node.content}
              createdOn={elem.node.createdOn}
              commentsCount={elem.node.commentsCount}
              likes={elem.node.likes}
              views={elem.node.views}
              // author={elem.node.author}
              tags={['Разработка', 'JavaScript', 'Браузеры']}
              preview={elem.node.preview}
              onEdit={() => history.push(`/edit/${elem.node.id}`)}
              // onPublish={() => ({})}
              moderateComment={'нарушает NDA'}
              moderateStatus={elem.node.status !== "MODERATION" ? 'rejected' : 'moderation'}
            />
          )}
        </div>
        <div>
          <RightBlock/>
        </div>
      </StreamTable>}
      {currentTab === 4 && (
        <BankContainer>
          <Interested title={'Соглашение Эйнштейна и einsum'} count={390}/>
          <Interested
            title={'Пятерка USB-микрофонов среднего ценового сегмента — для игровых стримов, интервью и творчества'}
            count={390}/>
          <Interested title={'Два поражения Microsoft или как работать без торговой марки в 21 веке'} count={390}/>
          <Interested
            title={'Торговая сеть DNS распродала все карты RTX 3060 до официального начала продаж «из-за технической ошибки»'}
            count={390}/>
        </BankContainer>
      )}

    </Container>
  )
}
