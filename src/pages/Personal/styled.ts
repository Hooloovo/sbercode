import styled from "styled-components";
import { PALETTE, TEXT } from "../../styles/constants";

interface ITab {
  active?: boolean,
}

interface ISearchTab {
  active?: boolean,
}

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const Header = styled.header`
  width: 100%;
  height: 83px;
  display: flex;
  align-items: baseline;
  border-bottom: 1px solid ${PALETTE.lines};
  margin-bottom: 16px;
`

export const HeaderContent = styled.div`
  width: 100%;
  height: 100%;
  padding: 38px 88px 19px 16px;
  display: flex;
  align-items: center;;
  justify-content: space-between;
  box-sizing: border-box;
`

export const TabsContainer = styled.div`
  // width: 100%;
  height: 100%;
  display: flex;
  align-items: baseline;
`

export const MainTab = styled.button<ITab>`
  border: none;
  background: transparent;
  font-style: normal;
  font-weight: ${props => props.active ? 'bold' : 'normal'};
  font-size: ${props => props.active ? '24px' : '16px'};
  line-height: ${props => props.active ? '29px' : '19px'};
  color: ${TEXT.black};
  margin-right: 25px;
  cursor: pointer;
`

export const SearchTab = styled.button<ISearchTab>`
  height: 28px;
  border: none;
  background-color: ${props => props.active ? PALETTE.blue : 'transparent'};
  border-radius: 14px;
  display: flex;
  flex-direction: column;
  color: ${props => props.active ? '#FFFFFF' : TEXT.black};
  font-style: normal;
  font-weight: ${props => props.active ? 'bold' : 'normal'};
  font-size: 16px;
  line-height: 19px;
  padding: 4px 16px;
  margin-right: 16px;
  cursor: pointer;
`

export const SearchContainer = styled.div`
  width: 100%;
  padding: 16px 88px 18px 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
  
  & > div {
    display: flex;
    align-items: center;
  }
`

export const StreamTable = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 0 88px 20px 16px;
  display: grid;
  grid-template-columns: 1fr 304px;
  grid-column-gap: 16px;
  
  & > div {
    display: flex;
    flex-direction: column;
  }
`

export const BankContainer = styled.div`
  width: 100%;
  box-sizing: border-box;
  padding: 0 88px 20px 16px;
`

export const CreateArticleButton = styled.button`
  height: 40px;
  padding: 10px 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  background: ${PALETTE.blue};
  color: #FFF;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 19px;
  cursor: pointer;
  border-radius: 4px;
`
