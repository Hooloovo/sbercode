import React from 'react';
import { Nav } from "../components/Nav";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { StreamPage } from "./Stream";
import { ApolloClient, InMemoryCache, ApolloProvider, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { PersonalPage } from "./Personal";
import { EditPage } from "./Edit";
import { CreatePage } from "./Create";
import { ArticlePage } from "./Article";
import { GiftPage } from "./Gifts";

const token = 'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImFncml0c2FpaSIsImV4cCI6MTYxNTcxNTU4Niwib3JpZ0lhdCI6MTYxNDUwNTk4Nn0.Y3G43AVA_6iS6KXXnaUSF3CE_Ec94OI5qnoMJLBOsT8'

const httpLink = createHttpLink({
  uri: 'http://93.188.167.199:8000/graphql/',
});

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      Authorization: token,
    }
  }
});

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache()
});

function App() {
  return (
    <div style={{ display: 'flex' }}>
      <ApolloProvider client={client}>
        <Router>
          <Nav/>
          <Switch>
            <Route path={'/posts'}>
              <PersonalPage/>
            </Route>
            <Route path={'/create'}>
              <CreatePage />
            </Route>
            <Route path={'/edit/:id'}>
              <EditPage />
            </Route>
            <Route path={'/post/:id'}>
              <ArticlePage />
            </Route>
            <Route path={'/gifts'}>
              <GiftPage />
            </Route>
            <Route path={'/'}>
              <StreamPage/>
            </Route>
          </Switch>
        </Router>
      </ApolloProvider>
    </div>
  );
}

export default App;
