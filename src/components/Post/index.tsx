import React from "react";
import { SimpleContainer } from "../basic/Container/styled";
import {
  Avatar,
  BottomContainer, ButtonsContainer,
  Container,
  Count,
  Description, EditButton,
  LinkToPost,
  MetaContainer, ModerateComment, ModerateContainer, ModerateStatus,
  Name, PublishButton, RemoveButton,
  Tags,
  Title
} from "./styled";
import { Eye, Like, Comment, Money, Trash, Publish, Pen } from "../../static/icons";


interface IComponentProps {
  id: string,
  title: string,
  content: string,
  preview?: string,
  createdOn: string,
  commentsCount?: number,
  likes?: number,
  views?: number,
  author?: any,
  tags: Array<string>,
  points?: number,
  onDelete?: any,
  onPublish?: any,
  onEdit?: any,
  moderateStatus?: string,
  moderateComment?: string,
}

export const Post: React.FC<IComponentProps> = ({
                                                  id,
                                                  title,
                                                  commentsCount,
                                                  content,
                                                  createdOn,
                                                  likes,
                                                  views,
                                                  author,
                                                  tags,
                                                  points,
                                                  onDelete,
                                                  onPublish,
                                                  onEdit,
                                                  moderateComment,
                                                  moderateStatus,
  preview
                                                }) => {
  return (
    <Container>
      {moderateStatus && <ModerateContainer>
        <ModerateStatus rejected={moderateStatus === 'rejected'}>
          {moderateStatus === 'rejected' ? 'Прости, модерацию не прошло' : 'Ожидает модерации'}
        </ModerateStatus>
        {moderateStatus === 'rejected' && <ModerateComment>
          Комментарий модератора:
          <ModerateComment style={{ fontWeight: 'normal' }}> {moderateComment}</ModerateComment>
        </ModerateComment>}
      </ModerateContainer>}
      {author && <MetaContainer>
        <div>
          <Avatar
            src={'https://s3-alpha-sig.figma.com/img/cbfe/b078/3bb2da08b26bd3d79f0fc03b7938dcda?Expires=1615161600&Signature=Z-YPxJpBAiKEkH1v7MtykIakAyKJo5z7y95KRR4afV~OYCIUQby-VY1DDK~8lcBdflT5~8FL-clfsZ1Gd9dmnUOoqkgCFwwDNb3lUaH0jOag18Gv2~-5pCqxLcwmkHPqbH3uQMWJHC-3IcTp669gSlDDmuLloBqx~iNPld3jZPvL-iuCMUi0RnMDZ4tFHOkBhcuO2GDVjuGfrZl44pqLSo~-8o6VkVL4Re~e4VUBFSSmdgW-p8lyZ4BG-omTUsSdpSQZOeAvS1tI9VwLg3q5s1zu3let2hd5HDvduqnNlwD~j4Z6r6sOb0ZhQlGXfXZ~CN4Y42YzwkPy4KfXlv2nYg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'}/>
          <Name>{author.username}</Name>
        </div>
        <div>
          <Count>
            <Eye/>
            <span>{views}</span>
          </Count>
          <Count>
            <Like/>
            <span>{likes}</span>
          </Count>
          <Count>
            <Comment/>
            <span>{commentsCount}</span>
          </Count>
          {points !== undefined && <Count>
            <Money/>
            <span>{points}</span>
          </Count>}
        </div>
      </MetaContainer>}
      <Title>{title}</Title>
      <Tags>{tags.join(', ')}</Tags>
      <Description>{preview !== undefined ? preview : ''}</Description>


      {!onPublish && !onEdit && <BottomContainer>
        <LinkToPost to={`/post/${id}`}>Читать дальше</LinkToPost>
        {onDelete && <RemoveButton onClick={() => onDelete()}><Trash/>Снять с публикации</RemoveButton>}
      </BottomContainer>}


      {(onPublish || onEdit) && <BottomContainer>
        <LinkToPost to={`/post/${id}`}>Читать дальше</LinkToPost>
        <ButtonsContainer>
          {onPublish && <PublishButton onClick={() => onPublish()}><Publish/>Опубликовать</PublishButton>}
          {onEdit && <EditButton onClick={() => onEdit()}><Pen/>Редактировать</EditButton>}
        </ButtonsContainer>
      </BottomContainer>}
    </Container>
  )
}
