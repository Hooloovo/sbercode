import styled from 'styled-components'
import { SHADOW } from "../../../styles/constants";

interface IComponentProps {
  isForm?: boolean,
}

export const SimpleContainer = styled.div<IComponentProps>`
  width: 100%;
  padding: 24px 24px ${props => props.isForm ? '0' : '24px'} 24px;
  box-shadow: ${SHADOW.shadow};
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
  border-radius: 5px;
  border: 1px solid #E0E0E0;
`;
