import styled from "styled-components";
import { PALETTE, TEXT } from "../../../styles/constants";

interface IComponentProps {
  inactive: boolean,
}

export const Button = styled.button<IComponentProps>`
  height: 40px;
  border: none;
  background-color: ${props => props.inactive ? TEXT.gray : PALETTE.blue};
  color: #FFF;
  padding: 11px 16px;
`;
