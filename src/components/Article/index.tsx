import React, { useState } from "react";
import { SimpleContainer } from "../basic/Container/styled";
import {
  Avatar,
  BottomContainer, ButtonsContainer, CommentButton, CommentContainer, CommentInput, CommentText, CommentTitle,
  Container,
  Count,
  Description, EditButton, FlexBlock, Hr,
  LinkToPost,
  MetaContainer, ModerateComment, ModerateContainer, ModerateStatus,
  Name, PublishButton, RemoveButton,
  Tags,
  Title
} from "./styled";
import { Eye, Like, Comment, Money, Trash, Publish, Pen } from "../../static/icons";
import { convertFromRaw, Editor, EditorState } from "draft-js";
import { useParams } from "react-router-dom";
import { useMutation, useQuery } from "@apollo/client";
import { CREATE_OFFER, GET_ARTICLE, MAKE_LIKE } from "../../api";


interface IComponentProps {

  preview?: string,
  commentsCount?: number,
  likes?: number,
  views?: number,
  author?: any,
  points?: number,
  onDelete?: any,
  onPublish?: any,
  onEdit?: any,
  moderateStatus?: string,
  moderateComment?: string,
}

export const Article: React.FC<IComponentProps> = ({
                                                     commentsCount,
                                                     likes,
                                                     views,
                                                     author,
                                                   }) => {
  // @ts-ignore
  let { id } = useParams();
  const { loading, error, data, refetch: refetchBlack } = useQuery(GET_ARTICLE, { variables: { id: id } });
  const [comment, setComment] = useState('');
  let tags = ['Разработка', 'JavaScript', 'Браузеры']
  const [makeLike, { data: anotherData }] = useMutation(MAKE_LIKE);

  return (
    <>
      {!loading && <Container>
        <MetaContainer>
          <div>
            <Avatar
              src={'https://s3-alpha-sig.figma.com/img/cbfe/b078/3bb2da08b26bd3d79f0fc03b7938dcda?Expires=1615161600&Signature=Z-YPxJpBAiKEkH1v7MtykIakAyKJo5z7y95KRR4afV~OYCIUQby-VY1DDK~8lcBdflT5~8FL-clfsZ1Gd9dmnUOoqkgCFwwDNb3lUaH0jOag18Gv2~-5pCqxLcwmkHPqbH3uQMWJHC-3IcTp669gSlDDmuLloBqx~iNPld3jZPvL-iuCMUi0RnMDZ4tFHOkBhcuO2GDVjuGfrZl44pqLSo~-8o6VkVL4Re~e4VUBFSSmdgW-p8lyZ4BG-omTUsSdpSQZOeAvS1tI9VwLg3q5s1zu3let2hd5HDvduqnNlwD~j4Z6r6sOb0ZhQlGXfXZ~CN4Y42YzwkPy4KfXlv2nYg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'}/>
            <Name>{data.getPost.author.username}</Name>
          </div>
          <div>
            <Count>
              <Eye/>
              <span>{data.getPost.views}</span>
            </Count>
            <Count>
              <Like/>
              <span>{data.getPost.likes}</span>
            </Count>
            <Count>
              <Comment/>
              <span>{data.getPost.commentsCount}</span>
            </Count>
          </div>
        </MetaContainer>
        <Title>{data.getPost.title}</Title>
        <Tags>{tags.join(', ')}</Tags>
        <div>
          {!loading &&
          <Editor
            onChange={() => ({})}
            editorState={EditorState.createWithContent(convertFromRaw(JSON.parse(data.getPost.content)))}
            readOnly={true}
          />}
        </div>
        <FlexBlock>
          <Like style={{margin: '32px 0', cursor: 'pointer'}} onClick={() => {
            makeLike({variables: {postId: id}})
              .then(() => refetchBlack())
          }} />
          <CommentTitle style={{ color: '#6E8FED' }} onClick={() => {
            makeLike({variables: { postId: id }})
              .then(() => refetchBlack())
          }}>{data.getPost.likes}</CommentTitle>
        </FlexBlock>
        <FlexBlock>
          <CommentTitle>Комментарии <CommentTitle style={{ color: '#6E8FED' }}>{data.getPost.commentsCount}</CommentTitle></CommentTitle>
        </FlexBlock>
        <Hr/>
        <CommentInput placeholder={'Ваш комментарий'} value={comment}
                      onChange={({ target }) => setComment(target.value)}/>
        <CommentButton dis={comment === ''}>Отправить</CommentButton>
        <CommentContainer>
          <div>
            <Avatar
              src={'https://s3-alpha-sig.figma.com/img/cbfe/b078/3bb2da08b26bd3d79f0fc03b7938dcda?Expires=1615161600&Signature=Z-YPxJpBAiKEkH1v7MtykIakAyKJo5z7y95KRR4afV~OYCIUQby-VY1DDK~8lcBdflT5~8FL-clfsZ1Gd9dmnUOoqkgCFwwDNb3lUaH0jOag18Gv2~-5pCqxLcwmkHPqbH3uQMWJHC-3IcTp669gSlDDmuLloBqx~iNPld3jZPvL-iuCMUi0RnMDZ4tFHOkBhcuO2GDVjuGfrZl44pqLSo~-8o6VkVL4Re~e4VUBFSSmdgW-p8lyZ4BG-omTUsSdpSQZOeAvS1tI9VwLg3q5s1zu3let2hd5HDvduqnNlwD~j4Z6r6sOb0ZhQlGXfXZ~CN4Y42YzwkPy4KfXlv2nYg__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA'}/>
            <Name>{data.getPost.author.username}</Name>
          </div>
          <CommentText>Решил я делать свой пет-проект по учету прочитанных книг на PWA. Покорять новые технологии и все
            такое. Расчет был на то, что с его выложу и установлю на телефон и вот у меня есть мобильное приложение,
            которое можно использовать оффлайн. Хочу я сгенерировать UUID, чтобы сохранить книгу, а не нахожу API.
            Предлагаю разобраться почему.</CommentText>
        </CommentContainer>
      </Container>}
    </>
  )
}
