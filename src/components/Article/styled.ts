import styled from "styled-components";
import { PALETTE, TEXT } from "../../styles/constants";
import { Link } from "react-router-dom";
import { SimpleContainer } from "../basic/Container/styled";

interface IComponentProps {

}

export const Container = styled(SimpleContainer)`
  margin-bottom: 16px;
`

export const MetaContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 16px;
  
  > div {
    display: flex;
    align-items: center;
  }
`

export const Avatar = styled.img`
  width: 24px;
  height: 24px;
  object-fit: cover;
  border-radius: 12px;
  margin-right: 5px;
`

export const Name = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.darkGray};
`

export const Count = styled.div`
  display: flex;
  align-items: center;
  margin-left: 16px;
  
  > span {
    margin-left: 4px;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 17px;
    color: ${PALETTE.blue};
  }
`

export const Title = styled.h2`
  width: 100%;
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 36px;
  color: ${TEXT.black};
  margin-bottom: 8px;
  margin-top: 0;
`

export const Tags = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.gray};
  margin-bottom: 16px;
`

export const Description = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 18px;
  color: ${TEXT.black};
  margin-bottom: 11px;
`

export const LinkToPost = styled(Link)`
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 15px;
  color: #6E8FED;
  text-decoration: none;
`

export const BottomContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const RemoveButton = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;
  width: 155px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 15px;
  color: ${PALETTE.red};
`

export const ButtonsContainer = styled.div`
  display: flex;
  align-items: center;
`

export const PublishButton = styled.button`
  width: 120px;
  background-color: transparent;
  border: none;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 15px;
  color: ${PALETTE.blue};
`

export const EditButton = styled.button`
  width: 127px;
  background-color: transparent;
  border: none;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 15px;
  color: ${TEXT.gray};
  margin-left: 16px;
`

export const ModerateContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

interface IModerateStatus {
  rejected?: boolean,
}

export const ModerateStatus = styled.span<IModerateStatus>`
  height: 24px;
  padding: 4px 13px;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 16px;
  color: #FFF;
  margin-bottom: 16px;
  border-radius: 12px;
  background-color: ${props => props.rejected ? PALETTE.red : TEXT.gray};
  box-sizing: border-box;
  width: fit-content;
`

export const ModerateComment = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 18px;
  margin-bottom: 16px;
  color: ${TEXT.gray};
`

export const FlexBlock = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
`

export const CommentTitle = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 22px;
  line-height: 27px;
  color: ${TEXT.black};
`

export const Hr = styled.div`
  width: 100%;
  margin-top: 10px;
  margin-bottom: 24px;
  content: '';
  border: 1px solid ${PALETTE.lines};
`

export const CommentInput = styled.textarea`
  width: 100%;
  height: 60px;
  border: 1px solid #E0E0E0;
  box-sizing: border-box;
  box-shadow: inset 0px 0px 8px rgba(36, 38, 41, 0.04);
  border-radius: 5px;
  margin-bottom: 16px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  color: ${TEXT.black};
`

interface IButton {
  dis?: boolean,
}

export const CommentButton = styled.button<IButton>`
  height: 40px;
  padding: 6px 34px;
  box-sizing: border-box;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 28px;
  color: #FFF;
  background: ${props => props.dis ? PALETTE.gray : PALETTE.blue};
  border-radius: 4px;
  margin-bottom: 32px;
  width: fit-content;
  border: none;
  cursor: pointer;
`

export const CommentContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const CommentText = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.black};
  margin-top: 8px;
`


