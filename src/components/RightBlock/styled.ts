import styled from 'styled-components';
import { SimpleContainer } from "../basic/Container/styled";
import { PALETTE, TEXT } from "../../styles/constants";

interface IMetricTab {
  active?: boolean
}

export const MetricContainer = styled(SimpleContainer)`
  padding-left: 0;
  padding-right: 0;
  
  > div {
    width: 100%;
    box-sizing: border-box;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 0 24px;
    margin-bottom: 32px;
  }
`

export const MetricTab = styled.button<IMetricTab>`
  border: none;
  height: 24px;
  padding: ${props => props.active ? '4px 7px' : '0'};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 12px;
  
  background-color: ${props => props.active ? PALETTE.blue : 'transparent'};
  color: ${props => props.active ? '#FFF' : TEXT.black};
  font-style: normal;
  font-weight: ${props => props.active ? 'bold' : 'normal'};
  font-size: 14px;
  line-height: 16px;
  cursor: pointer;
  
  margin-bottom: 32px;
`;

export const Count = styled.span`
width: 72px
  font-style: normal;
font-weight: bold;
font-size: 24px;
line-height: 29px;

color: #6E8FED;
`

export const Title = styled.span`
width: 170px;
  font-style: normal;
font-weight: bold;
font-size: 14px;
line-height: 18px;
color: #828282;
`

export const MetricCount = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 36px;
  color: ${PALETTE.red};
  margin-bottom: 4px;
`

export const MDesc = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.gray};
  
  margin-bottom: 32px;
`

export const PointText = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.black};
  margin-bottom: 24px;
`

export const PointName = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  color: ${TEXT.black};
  margin-bottom: 8px;
`

export const PointPercent = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  color: ${TEXT.gray};
  margin-bottom: 8px;
`

export const MetricLine = styled.img`
  width: 256px;
  height: 10px;
  margin-bottom: 16px;
`
