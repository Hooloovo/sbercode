import React, { useState } from 'react';
import {
  Count,
  MDesc, MetricContainer,
  MetricCount,
  MetricLine,
  MetricTab,
  PointName,
  PointPercent,
  PointText, Title
} from "./styled";
import { ReactComponent as FilterIcon } from "../../pages/Stream/img/filter.svg";
import metric1 from "../../pages/Stream/img/metric.svg";
import metric2 from "../../pages/Stream/img/metric2.svg";
import { useQuery } from "@apollo/client";
import { GET_ARTICLE, GET_MY_OFFERS } from "../../api";

export const RightBlock = () => {
  const [metricTab, setMetricTab] = useState(1);
  const { loading, error, data, refetch: refetchBlack } = useQuery(GET_MY_OFFERS);
  return (
    <MetricContainer>
      <div>
        <MetricTab active={true}>Все хотят, чтобы ТЫ написал про:</MetricTab>
      </div>
      {!loading && data.myOffers.edges.map(
        // @ts-ignore
        elem =>
        <div>
          <Count>{elem.node.votes} человек
            хотят</Count>
          <Title>{elem.node.title}</Title>
        </div>)}
    </MetricContainer>
  )
}
