import React from "react";
import styled from "styled-components";
import { SimpleContainer } from "../basic/Container/styled";
import { PALETTE, TEXT } from "../../styles/constants";

interface IInput extends React.HTMLAttributes<HTMLSpanElement> {
  focused?: boolean,
}

export const Container = styled(SimpleContainer)`
  position: relative;
  padding: 56px 27px;
  min-height: 314px;
  margin-bottom: 16px;
`

export const Title = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 36px;
  color: ${TEXT.black};
  margin-bottom: 16px;
`

export const InputContainer = styled.div`
  width: 470px;
  position: relative;
  border-bottom: 1px solid ${TEXT.darkGray};
  margin-bottom: 16px;
`

export const Input = styled.span<IInput>`
  display: block;
  outline: none;
  width: 278px;
  min-height: 24px;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: ${props => !props.focused ? TEXT.gray : TEXT.black}};
`

export const Girl = styled.img`
  position: absolute;
  bottom: 0;
  right: 0;
`

export const Boy = styled.img`
  position: absolute;
  bottom: 0;
  right: 0;
`

export const SubmitButton = styled.button`
  border: none;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 32px;
  color: ${PALETTE.blue};
  background-color: transparent;
  width: fit-content;
  cursor: pointer;
`

