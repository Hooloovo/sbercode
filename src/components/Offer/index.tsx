import React, { useState, useRef } from "react";
import { Title, Container, InputContainer, Input, Boy, Girl, SubmitButton } from "./styled";
import boy from './img/boy.svg';
import girl from './img/girl.svg';
import { useMutation } from "@apollo/client";
import { CREATE_OFFER } from "../../api";

interface IComponentProps {

}

export const Offer: React.FC<IComponentProps> = () => {
  const [focused, setFocused] = useState(false);
  const [filled, setFilled] = useState(false);
  const [val, setVal] = useState<string>('');
  const ref = useRef<HTMLSpanElement>();
  const [createOffer, { data: anotherData }] = useMutation(CREATE_OFFER);

  const handleBlur = () => {
    // @ts-ignore
    if (ref && ref.current !== undefined && ref.current.innerHTML !== 'Напишите ваш вариант' && ref.current.innerHTML !== '') {
      setVal(ref.current.innerHTML.toString())
      setFilled(true)
    } else {
      setFocused(false)
      setFilled(false)
    }
  }

  // @ts-ignore
  return (
    <Container>
      <Title>Хочу прочитать про...</Title>
      <InputContainer>
        <Input
          // @ts-ignore
          ref={ref}
          contentEditable
          role="textbox"
          placeholder={'Напишите ваш вариант'}
          focused={focused}
          onFocus={() => setFocused(true)}
          onBlur={() => handleBlur()}
          onChange={(e) => console.log(e)}
        >{!focused && !filled && 'Напишите ваш вариант'}</Input>
        <Boy src={boy} />
      </InputContainer>
      <SubmitButton
        // @ts-ignore
        onClick={() => {
          createOffer({variables: {title: val}})
          // @ts-ignore
          ref.current.innerHTML = '';
        }}>Отправить</SubmitButton>
      <Girl src={girl} />
    </Container>
  )
}
