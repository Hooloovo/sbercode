import React, { useState } from "react";
import { Container, InputTags, InputTitle, Hr, TabContainer, Tab, Button } from "./styled";
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { convertFromRaw, convertToRaw, EditorState } from 'draft-js';
import { useMutation } from "@apollo/client";
import { ADD_ARTICLE, PUBLISH_ARTICLE, UPDATE_ARTICLE } from "../../api";
import { useHistory, useLocation } from "react-router-dom";

interface IComponentProps {
  handleChange: (val: any) => void,
  currentState?: any,
}

interface IEditor {
  content?: any,
  mainTitle?:any,
}

class ControlledEditor extends React.Component<IComponentProps> {
  constructor(props: any) {
    super(props);
    this.state = {
      editorState: props.currentState ? EditorState.createWithContent(convertFromRaw(props.currentState)) : EditorState.createEmpty(),
    };
  }

  onEditorStateChange: Function = (editorState: any) => {
    // @ts-ignore
    const { handleChange } = this.props;
    const contentState = editorState.getCurrentContent();
    this.setState({
      editorState,
    });
    handleChange(convertToRaw(contentState));
  };

  render() {
    // @ts-ignore
    const { editorState } = this.state;
    return (
      <Editor
        editorState={editorState}
        // @ts-ignore
        onEditorStateChange={this.onEditorStateChange}
      />
    )
  }
}

export const EditPost: React.FC<IEditor> = ({ content, mainTitle }) => {
  const [currentTab, setTab] = useState(1)
  const [state, setState] = useState(content !== undefined ? content : '')
  const [title, setTitle] = useState<string>(mainTitle!== undefined ? mainTitle : '')
  const [addArticle, { data }] = useMutation(ADD_ARTICLE);
  const [updateArticle, { data: someData }] = useMutation(UPDATE_ARTICLE);
  const [publishArticle, { data: anotherData }] = useMutation(PUBLISH_ARTICLE);
  const history = useHistory();
  const location = useLocation();

  const onCreate = () => {
    let preview = state.blocks.length > 0 ? state.blocks[0].text : '';
    if (location.pathname.startsWith('/edit')) {
      updateArticle({ variables: {
        title: title,
          content: JSON.stringify(state),
          preview,
          postId: location.pathname.split('/')[location.pathname.split('/').length - 1]
      }})
        .then(() => history.push('/posts'))
    } else {
      addArticle({ variables: { title: title, content: JSON.stringify(state), preview}})
        .then(() => history.push('/posts'))
    }
  }

  const onPublish = () => {
    if (location.pathname.startsWith('/edit')) {
      publishArticle({ variables: {
          postId: location.pathname.split('/')[location.pathname.split('/').length - 1]
        }})
        .then(() => history.push('/posts'))
    } else {
      let preview = state.blocks.length > 0 ? state.blocks[0].text : '';
      addArticle({ variables: { title: title, content: JSON.stringify(state), preview}})
        .then(({data}) => publishArticle({ variables: {
            postId: data.createPost.post.id
          }}))
        .then(() => history.push('/posts'))
    }
  }

  return (
    <Container>
      <InputTitle placeholder={'Заголовок'} value={title} onChange={({target}) => setTitle(target.value)} />
      <div>
        <ControlledEditor handleChange={(val) => setState(val)} currentState={state} />
      </div>
      <Hr />
      <InputTags placeholder={'Добавьте теги'}/>
      <TabContainer>
        <Tab onClick={() => setTab(1)} active={currentTab === 1}>Дизайн</Tab>
        <Tab onClick={() => setTab(2)} active={currentTab === 2}>Менеджмент</Tab>
        <Tab onClick={() => setTab(3)} active={currentTab === 3}>Front</Tab>
        <Tab onClick={() => setTab(4)} active={currentTab === 4}>Back</Tab>
        <Tab onClick={() => setTab(5)} active={currentTab === 5}>DevOps</Tab>
        <Tab onClick={() => setTab(6)} active={currentTab === 6}>Анализ данных</Tab>
        <Tab onClick={() => setTab(7)} active={currentTab === 7}>Soft Skills</Tab>
        <Tab onClick={() => setTab(8)} active={currentTab === 8}>SMM</Tab>
        <Tab onClick={() => setTab(9)} active={currentTab === 9}>Support</Tab>
        <Tab onClick={() => setTab(10)} active={currentTab === 10}>HR</Tab>
        <Tab onClick={() => setTab(11)} active={currentTab === 11}>Friday News</Tab>
      </TabContainer>
      <TabContainer>
        <Button onClick={() => onCreate()}>Сохранить в черновик</Button>
        <Button submit onClick={() => onPublish()}>Опубликовать</Button>
      </TabContainer>
    </Container>
  )
}
