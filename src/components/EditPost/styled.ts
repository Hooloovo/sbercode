import styled from "styled-components";
import { SimpleContainer } from "../basic/Container/styled";
import { PALETTE, TEXT } from "../../styles/constants";

export const Container = styled(SimpleContainer)`
  width: calc(100% - 54px);
  height: 100%;
  margin: 24px 27px 32px;
  box-sizing: border-box;
`

export const InputTitle = styled.input`
  width: 100%;
  border: none;
  border-bottom: 1px solid ${PALETTE.lines};
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 36px;
  color: ${TEXT.black};
  margin-bottom: 16px;
`

export const InputTags = styled.input`
  width: 100%;
  border: none;
  border-bottom: 1px solid ${PALETTE.lines};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.black};
  margin-top: 32px;
`

export const Hr = styled.div`
  width: 100%;
  border-bottom: 1px solid ${PALETTE.lines};
  content: '';
  margin-top: 40px;
`

export const TabContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: baseline;
  margin-top: 32px;
`

interface ITab {
  active?: boolean
}

export const Tab = styled.button<ITab>`
  height: 28px;
  box-sizing: border-box;
  padding: ${props => props.active ? '4px 16px' : '0'};
  border-radius: 14px;
  font-style: normal;
  font-weight: ${props => props.active ? 'bold' : 'normal'};
  font-size: 16px;
  line-height: 19px;
  background: ${props => props.active ? PALETTE.blue : 'transparent'};
  color: ${props => props.active ? '#FFF' : TEXT.black};
  margin-right: 16px;
  cursor: pointer;
  border: none;
`

interface IButton {
  submit?: boolean,
}

export const Button = styled.button<IButton>`
  height: 40px;
  display: flex;
  align-items: center;
  color: #FFF;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 19px;
  background: ${props => props.submit ? PALETTE.blue : PALETTE.yellow};
  border: none;
  border-radius: 4px;
  margin-right: 16px;
  padding: 0 16px;
  cursor: pointer;
`

