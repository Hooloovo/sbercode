import styled from "styled-components";
import { SimpleContainer } from "../basic/Container/styled";
import { PALETTE, TEXT } from "../../styles/constants";


export const Container = styled(SimpleContainer)`
  min-height: 103px;
  padding: 24px;
  display: flex;
  margin-bottom: 24px;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`

export const InfoContainer = styled.div`
  height: 100%;
  min-height: 55px;
  max-width: 666px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const Title = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 22px;
  color: ${TEXT.black};
`

export const Count = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: ${PALETTE.blue};
`

export const Button = styled.button`
  height: 40px;
  box-sizing: border-box;
  padding: 10px 16px;
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 19px;
  color: #FFF;
  border: none;
  border-radius: 4px;
  background: ${PALETTE.blue};
`
