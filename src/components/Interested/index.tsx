import React from "react";
import { Container, InfoContainer, Count, Button, Title } from "./styled";

interface IComponentProps {
  title: string,
  count: number,
}

export const Interested: React.FC<IComponentProps> = ({ title, count}) => {
  return (
    <Container>
      <InfoContainer>
        <Title>{title}</Title>
        <Count>Заинтересованных в статье: {count}</Count>
      </InfoContainer>
      <Button>Хочу написать про это</Button>
    </Container>
  )
}
