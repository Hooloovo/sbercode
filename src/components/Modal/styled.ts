import styled from 'styled-components';

export const Container = styled.div`
  width: 687px;
  height: 480px;
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: #FFF;
  border-radius: 8px;
  margin: auto;
  top: 100px;
`

export const Img = styled.img`
  position: absolute;
  top: 0;
`

export const Title = styled.span`
  width: 400px;
  margin-top: 214px;
  font-style: normal;
font-weight: bold;
font-size: 32px;
line-height: 32px;
text-align: center;
margin-bottom: 24px;
color: #333333;
`

export const Subtitle = styled.span`
  width: 400px; 
  font-style: normal;
font-weight: bold;
font-size: 18px;
line-height: 32px;
text-align: center;
margin-bottom: 16px;
color: #333333;
opacity: 0.8;
`

export const Button = styled.button`
  width: 311px;
height: 40px;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
background: #6E8FED;
border-radius: 4px;
font-style: normal;
font-weight: bold;
font-size: 14px;
line-height: 28px;
text-align: center;
letter-spacing: 0.2px;
color: #FFFFFF;
`
