import React from 'react';
import { Container, Img, Subtitle, Title, Button } from "./styled";
import img from './img/back.svg';
import { useHistory } from "react-router-dom";

export const ModalBlock = () => {
  const history = useHistory();
  return (
    <Container>
      <Img src={img} />
      <Title>1,204 человека
        хотят прочить ТВОЮ статью про</Title>
      <Subtitle>Создание универсального конструктора таблиц</Subtitle>
      <Button onClick={() => history.push('/create')}>Написать статью!</Button>
    </Container>
  )
}
