import React, { useState } from "react";
import { Button, ButtonsContainer, Container, Description, Image, TextContainer, Title } from "./styled";
import img from './img/img.png'
import { useMutation } from "@apollo/client";
import { CREATE_OFFER, MAKE_VOTE } from "../../api";

interface IComponentProps {
  title: string,
  id: string
}

export const Poll: React.FC<IComponentProps> = ({ title , id}) => {
  const [makeVote, { data: anotherData }] = useMutation(MAKE_VOTE);
  const [isVote, setVote] = useState(false)
  return !isVote ? (
    <Container>
      <Image src={img} />
      <TextContainer>
        <Description>Реши судьбу статьи</Description>
        <Title>{title}</Title>
      </TextContainer>
      <ButtonsContainer>
        <Button isAgree onClick={() => {
          makeVote({ variables: {id: id, type: "FOR"}})
          setVote(true)
        }}>Интересно</Button>
        <Button onClick={() => {
          makeVote({ variables: {id: id, type: "AGAINST"}})
          setVote(true)
        }}>Не, отстой</Button>
      </ButtonsContainer>
    </Container>
  ) : (<></>)
}
