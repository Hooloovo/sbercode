import styled from "styled-components";
import { SimpleContainer } from "../basic/Container/styled";
import { PALETTE, TEXT } from "../../styles/constants";

interface IButton {
  isAgree?: boolean,
}

export const Container = styled(SimpleContainer)`
  padding: 0;
  overflow: hidden;
  margin-bottom: 16px;
`

export const Image = styled.img`
  width: 100%;
  object-fit: contain;
`

export const TextContainer = styled.div`
  padding: 7px 27px 0;
  display: flex;
  flex-direction: column;
`

export const Description = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: ${TEXT.mediumGray};
  margin-bottom: 16px;
`

export const Title = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 32px;
  color: ${TEXT.black};
  margin-bottom: 27px;
`

export const ButtonsContainer = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 63px;
`

export const Button = styled.button<IButton>`
  border: none;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 32px;
  color: #FFFFFF;
  background-color: ${props => props.isAgree ? PALETTE.blue : PALETTE.red};
  cursor: pointer;
`
