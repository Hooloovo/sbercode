import styled from "styled-components";

export const Title = styled.h2`
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 36px;
`;

interface ITextProps {
  lineHeight: number,
  fontSize: number,
  bold: boolean,
}

export const Text = styled.span<ITextProps>`
  font-family: Inter;
  font-style: normal;
  font-size: ${props => props.fontSize}px;
  line-height: ${props => props.lineHeight}px;
`
