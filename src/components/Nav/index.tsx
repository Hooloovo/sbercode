import React from 'react'
import { Balance, BalanceContainer, Container, Navigation, NavTabs, PersonContainer } from './styled'
import avatar from './img/avatar.svg';
import { Gift, Money, Pen, Stream } from "../../static/icons";
import { useLocation } from 'react-router-dom';
import { useQuery } from "@apollo/client";
import { GET_POSTS, GET_USER_INFO } from "../../api";

interface IComponentProps {

}

export const Nav: React.FC<IComponentProps> = () => {
  const { pathname } = useLocation();
  const { loading, error, data } = useQuery(GET_USER_INFO);
  return (
    <Container>
      <PersonContainer>
        <img src={avatar} />
        <div>
          <span>Привет, Анастасия!</span>
          <BalanceContainer>
            <Money />
            {!loading && <Balance>{data.userInfo.balance}</Balance>}
          </BalanceContainer>
        </div>
      </PersonContainer>
      <Navigation>
        <NavTabs active={pathname === '/'} to={'/'}>
          <div>
            <Stream />
            <span>Поток</span>
          </div>
        </NavTabs>
        <NavTabs active={pathname === '/gifts'} to={'/gifts'}>
          <div>
            <Gift />
            <span>Подарочки</span>
          </div>
        </NavTabs>
        <NavTabs active={pathname === '/posts'} to={'/posts'}>
          <div>
            <Pen />
            <span>Мои статьи</span>
          </div>
        </NavTabs>
      </Navigation>
    </Container>
  )
}
