import styled from 'styled-components';
import { PALETTE, SHADOW, TEXT } from '../../styles/constants';
import { Link } from "react-router-dom";


interface ITabs {
  active?: boolean
}

export const Container = styled.aside`
  width: 384px;
  min-height: 100vh;
  box-shadow: ${SHADOW.shadow};
  display: flex;
  flex-direction: column;
`

export const PersonContainer = styled.div`
  height: 50px;
  margin-left: 108px;
  display: flex;
  align-items: center;
  padding: 24px 0;

  & > img {
    height: 50px;
    width: 50px;
    border-radius: 25px;
    margin-right: 16px;
  }

  & > div {
    display: flex;
    flex-direction: column;
    align-items: flex-start;

    > span {
      margin-bottom: 8px;
    }
  }
`

export const BalanceContainer = styled.div`
  display: flex;
  align-items: center;
`

export const Balance = styled.span`
  margin-left: 4px;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 19px;
  color: ${PALETTE.blue};
`

export const Navigation = styled.nav`
  width: 100%;
  display: flex;
  flex-direction: column;
`

export const NavTabs = styled(Link)<ITabs>`
  width: calc(100%-108px);
  height: 74px;
  background-color: ${props => props.active ? PALETTE.blue + '33' : 'transparent'};
  display: flex;
  align-items: center;
  padding-left: 108px;
  text-decoration: none;
  color: ${TEXT.black};
  
  & > div {
    display: flex;
    align-items: baseline;
    
    > span {
      font-style: normal;
      font-weight: ${props => props.active ? 'bold' : 'normal'};
      font-size: 18px;
      line-height: 22px;  
      color: ${props => props.active ? PALETTE.blue : PALETTE.gray};
      margin-left: 8px;
      text-decoration: none;
    }
    
    > svg > path {
      fill: ${props => props.active ? PALETTE.blue : PALETTE.gray}
    }
  }
  
  &:hover {
    background-color: ${PALETTE.blue}33;
  
  &:hover > div > span {
    font-weight: bold;
    color: ${PALETTE.blue};
  }
  
  &:hover > div > svg > path {
    fill: ${PALETTE.blue}
  }
`
