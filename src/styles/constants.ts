const PALETTE: any = {
  blue: '#6E8FED',
  yellow: '#F5B245',
  pink: '#FC8678',
  red: '#FF6A59',
  lines: '#E0E0E0',
}

const TEXT: any = {
  black: '#333333',
  darkGray: '#4F4F4F',
  mediumGray: '#828282',
  gray: '#BDBDBD',
}
const SHADOW: any = {
  shadow: '0px 2px 16px rgba(36, 38, 41, 0.21)',
}

export {
  PALETTE,
  TEXT,
  SHADOW
}
