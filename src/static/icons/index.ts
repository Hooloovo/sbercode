import { ReactComponent as Comment } from './comment.svg';
import { ReactComponent as Eye } from './eye.svg';
import { ReactComponent as Gift } from './gift.svg';
import { ReactComponent as Like } from './like.svg';
import { ReactComponent as Money } from './money.svg';
import { ReactComponent as Pen } from './pen.svg';
import { ReactComponent as Stream } from './stream.svg';
import { ReactComponent as Search } from './search.svg';
import { ReactComponent as Trash } from './trash.svg';
import { ReactComponent as Publish } from './publish.svg';


export {
  Comment,
  Eye,
  Gift,
  Like,
  Money,
  Pen,
  Stream,
  Search,
  Trash,
  Publish,
}
