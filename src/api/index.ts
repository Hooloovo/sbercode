import { gql } from '@apollo/client';

export const host: string = 'http://93.188.167.199:8000/graphql';

export const header: any = {
  Authorization:"JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InVzZXIxIiwiZXhwIjoxNjE0NDMyMTY5LCJvcmlnSWF0IjoxNjE0NDMxODY5fQ.KciM1EBbMlVGLIwStOOiqwZtjWTj8Z1R5StriuT2TEk"
}

export const GET_POSTS = gql`
query AllPosts {
  allPosts {
    edges {
      node {
        id
        ... on Post {
          id
          title
          preview
          content
          likes
          views
          isLiked
          commentsCount
          createdOn
          author {
            username
          }
        }
      }
    }
  }
}
`

export const ADD_ARTICLE = gql`
mutation CreatePost($title: String!, $content: String!, $preview: String) {
  createPost(postData: {title: $title, content: $content, preview: $preview}) {
    ok
    post {
      id
      title
      content
    }
  }
}
`

export const UPDATE_ARTICLE = gql`
mutation UpdatePost($title: String!, $content: String!, $preview: String, $postId: ID!) {
  updatePost(postData: {title: $title, content: $content, preview: $preview}, postId: $postId) {
    ok
    post {
      title
      content
    }
  }
}
`

export const PUBLISH_ARTICLE = gql`
mutation PublishPost($postId: ID!) {
  publishPost(postId: $postId) {
    ok
  }
}
`

export const GET_BLACKNOTES = gql`
query MyPosts {
  myPosts(status: ["DRAFT"]) {
    edges {
      node {
        id
        ... on Post {
          id
          title
          content
          preview
          likes
          views
          isLiked
          commentsCount
          createdOn
          author {
            username
          }
        }
      }
    }
  }
}
`

export const GET_MODERATION = gql`
query MyPosts {
  myPosts(status: ["MODERATION", "REJECTED"]) {
    edges {
      node {
        id
        ... on Post {
          id
          title
          content
          preview
          likes
          views
          isLiked
          commentsCount
          status
          createdOn
          author {
            username
          }
        }
      }
    }
  }
}
`

export const GET_PUBLISHED = gql`
query MyPosts {
  myPosts(status: ["PUBLISHED"]) {
    edges {
      node {
        id
        ... on Post {
          id
          title
          content
          preview
          likes
          views
          isLiked
          commentsCount
          createdOn
          author {
            username
          }
        }
      }
    }
  }
}
`

export const GET_ARTICLE = gql`
query GetPost($id: ID!){
  getPost(id: $id) {
    id
    title
    content
    likes
    views
    commentsCount
    author {
      username
    }
  }
}
`

export const GET_OFFER = gql`
query GetNextOffer{
  getNextOffer {
    id
    title
  }
}
`

export const CREATE_OFFER = gql`
mutation CreateOffer($title: String!) {
  createOffer(offerData: {title: $title}) {
    ok
  }
}
`

export const MAKE_LIKE = gql`
mutation MakeLike($postId: ID!) {
  makeLike(postId: $postId) {
    ok
  }
}
`

export const MAKE_VOTE = gql`
mutation MakeVote($id: ID!) {
  makeVote(offerId: $id, type: FOR) {
    ok
    offer {
      id
      title
      votes
    }
  }
}
`

export const GET_MY_OFFERS = gql`
query MyOffers{
  myOffers {
    pageInfo {
      startCursor
      endCursor
      hasNextPage
    }
    edges {
      cursor
      node {
          id
          title
          tookToWorkOn
          votes
        }
    }
  }
}
`

export const GET_USER_INFO = gql`
query UserInfo {
  userInfo {
    balance
  }
}
`

export const GET_STATISTICS = gql`
query Statistic {
  statistics {
    distance
  }
}
`
